﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ZedGraph;

namespace ASE_Assignment_B
{
    public partial class Form1 : Form
    {
        YAxis yAxisPower;
        Ride_Data file1 = new Ride_Data();
        Ride_Data file2 = new Ride_Data();

        public Form1()
        {
            InitializeComponent();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /// <summary> 
            /// Once the user has selected the file to open, 
            /// it then sends the file to the Ride_Data class, and ReadFile method
            /// Once the information has been returned, it is then send to other methods in this class to form the graph
            /// </summary>
            OpenFileDialog openFile = new OpenFileDialog();

            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                file1.ReadFile(openFile.InitialDirectory + openFile.FileName);
                //file1.Get_HeartRate_data();
                file1.UKUnitButton = UKUnitRadioButton.Checked;
                file1.USUnitButton = USUnitRadioButton.Checked;
                Graph(file1);
                bodyDataListView(file1);
                statisticsDataListView();
                comparisonList();
                GraphComparison1(file1);
            }



        }

        private void Graph(Ride_Data file1)
        {
            /// <summary> 
            /// This method takes the information from the Ride_Data, 
            /// creates a point pair list to store the graph data and place the data on the graph
            /// The code at the bottom of the method, changes the axis labels
            /// </summary>
            Cycle_Graph.GraphPane.CurveList.Clear();
            Cycle_Graph.Refresh();

            PointPairList heartRate_Data = file1.Get_HeartRate_data();
            //Cycle_Graph.GraphPane.AddCurve("Heart Rate", heartRate_Data, Color.Black, SymbolType.None);

            PointPairList speed_Data = file1.Get_Speed_data();
            //Cycle_Graph.GraphPane.AddCurve("Speed", speed_Data, Color.DarkRed, SymbolType.None);

            PointPairList cadence_Data = file1.Get_Cadence_data();
            //Cycle_Graph.GraphPane.AddCurve("Cadence", cadence_Data, Color.Blue, SymbolType.None);

            PointPairList altitude_Data = file1.Get_Altitude_data();
            //Cycle_Graph.GraphPane.AddCurve("Altitiude", altitude_Data, Color.DarkCyan, SymbolType.None);

            PointPairList power_Data = file1.Get_Power_data();
            //Cycle_Graph.GraphPane.AddCurve("Power", power_Data, Color.Khaki, SymbolType.None);

            PointPairList powerLeft_Data = file1.Get_PowerLeft_data();
            //Cycle_Graph.GraphPane.AddCurve("Left Leg Power", powerLeft_Data, Color.Yellow, SymbolType.None);

            PointPairList powerRight_Data = file1.Get_PowerRight_data();
            //Cycle_Graph.GraphPane.AddCurve("Right Leg Power", powerRight_Data, Color.Teal, SymbolType.None);

            PointPairList pedalIndex_Data = file1.Get_PedalIndex_data();
            //Cycle_Graph.GraphPane.AddCurve("Pedal Index", pedalIndex_Data, Color.Purple, SymbolType.None);


            yAxisPower = new YAxis("Power (Watts)");
            yAxisPower.Title.FontSpec.Size = 6;
            yAxisPower.Scale.FontSpec.Size = 6;
            //yAxisPower.Scale.IsVisible = false; 
            //yAxisPower.MajorGrid.IsVisible = false;
            Cycle_Graph.GraphPane.YAxisList.Add(yAxisPower);

            LineItem powerCurve = new LineItem("Power", power_Data, Color.DeepPink, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.YAxisList.IndexOf(yAxisPower) };
            Cycle_Graph.GraphPane.CurveList.Add(powerCurve);

            YAxis yAxisHeartRate = new YAxis("Heart Rate (rpm)");
            yAxisHeartRate.Title.FontSpec.Size = 6;
            yAxisHeartRate.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.YAxisList.Add(yAxisHeartRate);

            LineItem heartrateCurve = new LineItem("Heart Rate", heartRate_Data, Color.Black, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.YAxisList.IndexOf(yAxisHeartRate) };
            Cycle_Graph.GraphPane.CurveList.Add(heartrateCurve);

            YAxis yAxisSpeed = new YAxis("Speed Data (km/h)");
            yAxisSpeed.Title.FontSpec.Size = 6;
            yAxisSpeed.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.YAxisList.Add(yAxisSpeed);

            LineItem speedDataCurve = new LineItem("Speed Data", speed_Data, Color.DarkRed, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.YAxisList.IndexOf(yAxisSpeed) };
            Cycle_Graph.GraphPane.CurveList.Add(speedDataCurve);

            YAxis yAxisCadence = new YAxis("Cadence (rpm)");
            yAxisCadence.Title.FontSpec.Size = 6;
            yAxisCadence.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.YAxisList.Add(yAxisCadence);

            LineItem cadenceCurve = new LineItem("Cadence", cadence_Data, Color.Blue, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.YAxisList.IndexOf(yAxisCadence) };
            Cycle_Graph.GraphPane.CurveList.Add(cadenceCurve);

            YAxis yAxisAltitude = new YAxis("Altitude (m)");
            yAxisAltitude.Title.FontSpec.Size = 6;
            yAxisAltitude.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.YAxisList.Add(yAxisAltitude);

            LineItem altitudeCurve = new LineItem("Altitude", altitude_Data, Color.DarkCyan, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.YAxisList.IndexOf(yAxisAltitude) };
            Cycle_Graph.GraphPane.CurveList.Add(altitudeCurve);

            //y2 axis

            Y2Axis yAxisPowerLeft = new Y2Axis("Power Left (Watts)");
            yAxisPowerLeft.Title.FontSpec.Size = 6;
            yAxisPowerLeft.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.Y2AxisList.Add(yAxisPowerLeft);

            LineItem powerLeftCurve = new LineItem("Power Left", powerLeft_Data, Color.Yellow, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.Y2AxisList.IndexOf(yAxisPowerLeft) };
            powerLeftCurve.IsY2Axis = true;
            Cycle_Graph.GraphPane.CurveList.Add(powerLeftCurve);
            yAxisPowerLeft.IsVisible = true;

            Y2Axis yAxisPowerRight = new Y2Axis("Power Right (Watts)");
            yAxisPowerRight.Title.FontSpec.Size = 6;
            yAxisPowerRight.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.Y2AxisList.Add(yAxisPowerRight);

            LineItem powerRightCurve = new LineItem("Power Right", powerRight_Data, Color.Teal, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.Y2AxisList.IndexOf(yAxisPowerRight) };
            powerRightCurve.IsY2Axis = true;
            Cycle_Graph.GraphPane.CurveList.Add(powerRightCurve);
            yAxisPowerRight.IsVisible = true;

            Y2Axis yAxisPedalIndex = new Y2Axis("Pedal Index Data");
            yAxisPedalIndex.Title.FontSpec.Size = 6;
            yAxisPedalIndex.Scale.FontSpec.Size = 6;
            Cycle_Graph.GraphPane.Y2AxisList.Add(yAxisPedalIndex);

            LineItem pedalIndexCurve = new LineItem("Pedal Index Data", pedalIndex_Data, Color.Purple, SymbolType.None)
            { YAxisIndex = Cycle_Graph.GraphPane.Y2AxisList.IndexOf(yAxisPedalIndex) };
            pedalIndexCurve.IsY2Axis = true;
            Cycle_Graph.GraphPane.CurveList.Add(pedalIndexCurve);
            yAxisPedalIndex.IsVisible = true;

            AxisLabel xAxisLabel = new AxisLabel("Time (Minutes)", "Arial", 10, Color.Black, false, false, false);
            Cycle_Graph.GraphPane.XAxis.Title = xAxisLabel;
            AxisLabel yAxisLabel = new AxisLabel("Unit", "Arial", 10, Color.Black, false, false, false);
            Cycle_Graph.GraphPane.YAxis.Title = yAxisLabel;
            Cycle_Graph.GraphPane.Title.Text = "Duncan's Dynamic Data";

            Cycle_Graph.GraphPane.XAxis.Scale.Max = file1.list.Count / 60;
            Cycle_Graph.AxisChange();
            Cycle_Graph.Refresh();
        }

        private void bodyDataListView(Ride_Data newFile)
        {
            /// <summary> 
            /// This method is taking in the data from the Ride_Data class, and relaying out the information in a list view on the form
            /// There is a data array which contains the information which is looped arounnd to display
            /// The if statement within the for loop, is to determine which measurement of speed needs to be chosen and displayed.
            /// </summary>
            bodyDataView.Refresh();
            bodyDataView.Clear();

            bodyDataView.Columns.Add("Heart Rate: ", 100);
            bodyDataView.Columns.Add("Heart Rate %:", 100);
            bodyDataView.Columns.Add("Speed: ", 100);
            bodyDataView.Columns.Add("Cadence: ", 100);
            bodyDataView.Columns.Add("Altitude: ", 100);
            bodyDataView.Columns.Add("Power: ", 100);
            bodyDataView.Columns.Add("FTP:", 100);
            bodyDataView.Columns.Add("Left Leg Power:", 100);
            bodyDataView.Columns.Add("Right Leg Power:", 100);
            bodyDataView.Columns.Add("Pedal Index", 100);

            String[] dataArray = new String[10];

            foreach (HR_Data_Row item in newFile.HR_data())
            {

                dataArray[0] = Convert.ToString(item.heartRate + " bpm");
                dataArray[1] = Convert.ToString(item.percentage_HR + " %");

                if (UKUnitRadioButton.Checked == true)
                {
                    dataArray[2] = Convert.ToString(item.speed_km + " km/h");
                    newFile.UKUnitButton = true;
                }
                else if (USUnitRadioButton.Checked == true)
                {
                    dataArray[2] = Convert.ToString(item.speed_ms + " mph");
                    newFile.USUnitButton = true;
                }

                dataArray[3] = Convert.ToString(item.cadence + " rpm");

                if (UKUnitRadioButton.Checked == true)
                {
                    dataArray[4] = Convert.ToString(item.altitude_m + " metres");
                    newFile.UKUnitButton = true;
                }
                else if (USUnitRadioButton.Checked == true)
                {
                    dataArray[4] = Convert.ToString(item.altitude_ft + " feet");
                    newFile.USUnitButton = true;
                }

                dataArray[5] = Convert.ToString(item.power + " watts");
                dataArray[6] = Convert.ToString(item.percentage_ftp + "%");
                dataArray[7] = Convert.ToString(item.powerLeft);
                dataArray[8] = Convert.ToString(item.powerRight);
                dataArray[9] = Convert.ToString(item.pedalIndex);

                ListViewItem listitem = new ListViewItem(dataArray);
                bodyDataView.Items.Add(listitem);
            }

        }

        private void statisticsDataListView()
        {
            /// <summary> 
            /// This method is taking in the data from the Ride_Data class, 
            /// There is a data array which contains the statistic information which is looped around to display
            /// </summary>
            statisticsListView.Clear();
            statisticsListView.Refresh();

            statisticsListView.Columns.Add("Statistic: ", 200);
            statisticsListView.Columns.Add("Value: ", 100);

            String[] averageArray = new String[2];

            averageArray[0] = "Minimum Heart Rate:";
            averageArray[1] = Convert.ToString(file1.calculate_minimum_HeartRate(file1.graph_list));

            ListViewItem listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Average Heart Rate:";
            averageArray[1] = Convert.ToString(file1.calculate_average_HeartRate(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Maximum Heart Rate:";
            averageArray[1] = Convert.ToString(file1.calculate_maximum_HeartRate(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Average Speed:";
            averageArray[1] = Convert.ToString(file1.calculate_average_Speed(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Maximum Speed:";
            averageArray[1] = Convert.ToString(file1.calculate_maximum_Speed(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Average Power:";
            averageArray[1] = Convert.ToString(file1.calculate_average_Power(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Maximum Power:";
            averageArray[1] = Convert.ToString(file1.calculate_maximum_Power(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Average Altitude:";
            averageArray[1] = Convert.ToString(file1.calculate_average_Altitude(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Maximum Altitude:";
            averageArray[1] = Convert.ToString(file1.calculate_maximum_Altitude(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

            averageArray[0] = "Total Distance:";
            averageArray[1] = Convert.ToString(file1.calculate_total_distance(file1.graph_list));

            listitem = new ListViewItem(averageArray);
            statisticsListView.Items.Add(listitem);

        }

        private void UKUnitRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            /// <summary>
            /// Once the UK Unit radio button is clicked, all the methods are recalled and cleared.
            /// This is so the data can be recalculated in the UK Unit
            /// </summary>
            Graph(file1);
            bodyDataListView(file1);
            statisticsDataListView();
        }

        private void USUnitRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            /// <summary>
            /// Once the US Unit radio button is clicked, all the methods are recalled and cleared.
            /// This is so the data can be recalculated in the US Unit
            /// </summary>


            Graph(file1);
            bodyDataListView(file1);
            statisticsDataListView();
        }

        private void maxHR_numbox_ValueChanged(object sender, EventArgs e)
        {
            /// <summary>
            /// This takes the unit that was entered by the user, and sends it to the method in Ride_Data
            /// Once the method sends back the number, the bodyDataListView method is called to update the table on the form
            /// </summary>
            file1.calculate_HR_Percentage(maxHR_numbox.Value);
            bodyDataListView(file1);
        }

        private void ftp_numBox_ValueChanged(object sender, EventArgs e)
        {
            /// <summary>
            /// This takes the unit that was entered by the user, and sends it to the method in Ride_Data
            /// Once the method sends back the number, the bodyDataListView method is called to update the table on the form
            /// </summary>
            file1.calculate_FTP(ftp_numBox.Value);
            bodyDataListView(file1);

        }

        private void Cycle_Graph_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            ///<summary>
            ///The zoomevent takes the minumum and maximum from the y and x axis on the graph pane
            ///Takes this co-ordinates, calculates the data within the section
            ///Then calls the method which displays the statistics
            /// </summary>
            /// 
            file1.graph_list.Clear();
            int count = file1.list.Count;

            foreach (HR_Data_Row item in file1.list)
            {
                
                if (item.power < yAxisPower.Scale.Max && item.power > yAxisPower.Scale.Min && (item.count / 60) > Cycle_Graph.GraphPane.XAxis.Scale.Min && (item.count / 60) < Cycle_Graph.GraphPane.XAxis.Scale.Max)
                {
                    HR_Data_Row row;
                    row = item;
                    file1.graph_list.Add(row);
                }
                Console.WriteLine("testing");

            }

            statisticsDataListView();
        }

        private void openFileBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /// <summary> 
            /// Once the user has selected the other file to open, 
            /// it then sends the file to the Ride_Data class, and ReadFile method
            /// Once the information has been returned, it is then send to other methods in this class to form the graph
            /// </summary>
            OpenFileDialog openFile = new OpenFileDialog();

            if (openFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                file2.ReadFile(openFile.InitialDirectory + openFile.FileName);
                file2.UKUnitButton = UKUnitRadioButton.Checked;
                file2.USUnitButton = USUnitRadioButton.Checked;
                comparisonList();
                GraphComparison2(file2);


            }
        }

        private void comparisonList()
        {
            ///<summary>
            /// This section compares the stastics between the two files selected. 
            /// It displays each calculation for the files, in turn 
            /// The data changes depending on the zoom state of the file
            /// The data is stored within an array, through list items, then displayed on the graph pane. 
            /// This code can be simply copied and adapted for any purpose similar
            /// </summary>
           
            graphComparisonList.Clear();
            graphComparisonList.Refresh();

            graphComparisonList.Columns.Add("Title: ", 200);
            graphComparisonList.Columns.Add("Value: ", 100);

            String[] comparisonArray = new String[2];

            comparisonArray[0] = "File 1 - Minimum Heart Rate:";
            comparisonArray[1] = Convert.ToString(file1.calculate_minimum_HeartRate(file1.graph_list));

            ListViewItem comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Minimum Heart Rate:";
            comparisonArray[1] = Convert.ToString(file2.calculate_minimum_HeartRate(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Average Heart Rate:";
            comparisonArray[1] = Convert.ToString(file1.calculate_average_HeartRate(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Average Heart Rate:";
            comparisonArray[1] = Convert.ToString(file2.calculate_average_HeartRate(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Maximum Heart Rate:";
            comparisonArray[1] = Convert.ToString(file1.calculate_maximum_HeartRate(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Maximum Heart Rate:";
            comparisonArray[1] = Convert.ToString(file2.calculate_maximum_HeartRate(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Average Speed:";
            comparisonArray[1] = Convert.ToString(file1.calculate_average_Speed(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Average Speed:";
            comparisonArray[1] = Convert.ToString(file2.calculate_average_Speed(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Maximum Speed:";
            comparisonArray[1] = Convert.ToString(file1.calculate_maximum_Speed(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Maximum Speed:";
            comparisonArray[1] = Convert.ToString(file2.calculate_maximum_Speed(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Average Power:";
            comparisonArray[1] = Convert.ToString(file1.calculate_average_Power(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Average Power:";
            comparisonArray[1] = Convert.ToString(file2.calculate_average_Power(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Maximum Power:";
            comparisonArray[1] = Convert.ToString(file1.calculate_maximum_Power(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Maximum Power:";
            comparisonArray[1] = Convert.ToString(file2.calculate_maximum_Power(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Average Altitude:";
            comparisonArray[1] = Convert.ToString(file1.calculate_average_Altitude(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Average Altitude:";
            comparisonArray[1] = Convert.ToString(file2.calculate_average_Altitude(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Maximum Altitude:";
            comparisonArray[1] = Convert.ToString(file1.calculate_maximum_Altitude(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Maximum Altitude:";
            comparisonArray[1] = Convert.ToString(file2.calculate_maximum_Altitude(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Total Distance:";
            comparisonArray[1] = Convert.ToString(file1.calculate_total_distance(file1.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Total Distance:";
            comparisonArray[1] = Convert.ToString(file2.calculate_total_distance(file2.graph_list));

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            double IF_file1 = file1.calculate_Intensity_Factor(Convert.ToDouble(FTPenteredBox.Value), file1.calculate_Normalized_Power(file1.graph_list));
            double NP_file1 = file1.calculate_Normalized_Power(file1.graph_list);
            double TSS_file1 = file1.calculate_training_stress_score(IF_file1, file1.length.TotalSeconds, NP_file1, Convert.ToDouble(FTPenteredBox.Value));

            //double IF_file2 = file2.calculate_Intensity_Factor(Convert.ToDouble(FTPenteredBox.Value), file2.calculate_Normalized_Power(file2.graph_list));
            //double TSS_file2 = file2.calculate_training_stress_score(IF_file2, file2.length.TotalSeconds);
            // double NP_file2 = file1.calculate_Normalized_Power(file2.graph_list);

            comparisonArray[0] = "File 1 - Intensity Factor:";
            comparisonArray[1] = Convert.ToString(comparisonArray[0] = "File 1 - Intensity Factor:");
            comparisonArray[1] = Convert.ToString(IF_file1);

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            /*comparisonArray[0] = "File 2 - Intensity Factor:";
            comparisonArray[1] = Convert.ToString(IF_file2);

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist); */

            comparisonArray[0] = "File 1 - Training Stress Score:";
            comparisonArray[1] = Convert.ToString(TSS_file1);

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            /*comparisonArray[0] = "File 2 - Training Stress Score:";
            comparisonArray[1] = Convert.ToString(TSS_file2);

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist); */

            comparisonArray[0] = "File 1 - Normalized Power:";
            comparisonArray[1] = Convert.ToString(NP_file1);

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            /*comparisonArray[0] = "File 2 - Normalized Power:";
            comparisonArray[1] = Convert.ToString(NP_file2);

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist); */

            comparisonArray[0] = "File 1 - Left Leg Power:";
            comparisonArray[1] = Convert.ToString(file1.Get_PowerLeft_data());

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Left Leg Power:";
            comparisonArray[1] = Convert.ToString(file2.Get_PowerLeft_data());

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Right Leg Power:";
            comparisonArray[1] = Convert.ToString(file1.Get_PowerRight_data());

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Right Leg Power:";
            comparisonArray[1] = Convert.ToString(file2.Get_PowerRight_data());

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 1 - Pedal Index:";
            comparisonArray[1] = Convert.ToString(file1.Get_PedalIndex_data());

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);

            comparisonArray[0] = "File 2 - Pedal Index:";
            comparisonArray[1] = Convert.ToString(file2.Get_PedalIndex_data());

            comparisonlist = new ListViewItem(comparisonArray);
            graphComparisonList.Items.Add(comparisonlist);
        }

        private void GraphComparison1(Ride_Data file1)
        {
            ///<summary>
            ///This method collects the selected 'PointPairList' information about the first file 
            ///And displays the information on a graph through line items
            ///This method has multiple y axis for each section of the information 
            /// The bottom lines show customisation of the graph, showing the title File 1
            /// </summary>
            
            comparisonGraph1.GraphPane.CurveList.Clear();
            comparisonGraph1.Refresh();

            PointPairList heartRate_Data = file1.Get_HeartRate_data();

            PointPairList speed_Data = file1.Get_Speed_data();

            PointPairList power_Data = file1.Get_Power_data();

            yAxisPower = new YAxis("Power");
            yAxisPower.Title.FontSpec.Size = 6;
            yAxisPower.Scale.FontSpec.Size = 6;
            comparisonGraph1.GraphPane.YAxisList.Add(yAxisPower);

            LineItem powerCurve = new LineItem("Power", power_Data, Color.DeepPink, SymbolType.None)
            { YAxisIndex = comparisonGraph1.GraphPane.YAxisList.IndexOf(yAxisPower) };
            comparisonGraph1.GraphPane.CurveList.Add(powerCurve);
            

            YAxis yAxisHeartRate = new YAxis("Heart Rate (rpm)");
            yAxisHeartRate.Title.FontSpec.Size = 6;
            yAxisHeartRate.Scale.FontSpec.Size = 6;
            comparisonGraph1.GraphPane.YAxisList.Add(yAxisHeartRate);

            LineItem heartrateCurve = new LineItem("Heart Rate", heartRate_Data, Color.Black, SymbolType.None)
            { YAxisIndex = comparisonGraph1.GraphPane.YAxisList.IndexOf(yAxisHeartRate) };
            comparisonGraph1.GraphPane.CurveList.Add(heartrateCurve);

            YAxis yAxisSpeed = new YAxis("Speed Data");
            yAxisSpeed.Title.FontSpec.Size = 6;
            yAxisSpeed.Scale.FontSpec.Size = 6;
            comparisonGraph1.GraphPane.YAxisList.Add(yAxisSpeed);

            LineItem speedDataCurve = new LineItem("Speed Data", speed_Data, Color.Blue, SymbolType.None)
            { YAxisIndex = comparisonGraph1.GraphPane.YAxisList.IndexOf(yAxisSpeed) };
            comparisonGraph1.GraphPane.CurveList.Add(speedDataCurve);

            AxisLabel xAxisLabel = new AxisLabel("Time (Minutes)", "Arial", 10, Color.Black, false, false, false);
            comparisonGraph1.GraphPane.XAxis.Title = xAxisLabel;
            AxisLabel yAxisLabel = new AxisLabel("Unit", "Arial", 10, Color.Black, false, false, false);
            comparisonGraph1.GraphPane.YAxis.Title = yAxisLabel;
            comparisonGraph1.GraphPane.Title.Text = "File 1";

            comparisonGraph1.GraphPane.XAxis.Scale.Max = file1.list.Count / 60;
            comparisonGraph1.AxisChange();
            comparisonGraph1.Refresh();
        }

        private void GraphComparison2(Ride_Data file2)
        {
            ///<summary>
            ///This method collects the selected 'PointPairList' information about the second file 
            ///And displays the information on a graph through line items
            ///This method has multiple y axis for each section of the information 
            /// The bottom lines show customisation of the graph, showing the title File 2
            /// </summary>
            
            comparisonGraph2.GraphPane.CurveList.Clear();
            comparisonGraph2.Refresh();

            PointPairList heartRate_Data = file2.Get_HeartRate_data();

            PointPairList speed_Data = file2.Get_Speed_data();

            PointPairList power_Data = file2.Get_Power_data();

            yAxisPower = new YAxis("Power");
            yAxisPower.Title.FontSpec.Size = 6;
            yAxisPower.Scale.FontSpec.Size = 6;
            comparisonGraph2.GraphPane.YAxisList.Add(yAxisPower);

            LineItem powerCurve = new LineItem("Power", power_Data, Color.DeepPink, SymbolType.None)
            { YAxisIndex = comparisonGraph2.GraphPane.YAxisList.IndexOf(yAxisPower) };
            comparisonGraph2.GraphPane.CurveList.Add(powerCurve);

            YAxis yAxisHeartRate = new YAxis("Heart Rate (rpm)");
            yAxisHeartRate.Title.FontSpec.Size = 6;
            yAxisHeartRate.Scale.FontSpec.Size = 6;
            comparisonGraph2.GraphPane.YAxisList.Add(yAxisHeartRate);

            LineItem heartrateCurve = new LineItem("Heart Rate", heartRate_Data, Color.Black, SymbolType.None)
            { YAxisIndex = comparisonGraph2.GraphPane.YAxisList.IndexOf(yAxisHeartRate) };
            comparisonGraph2.GraphPane.CurveList.Add(heartrateCurve);

            YAxis yAxisSpeed = new YAxis("Speed Data");
            yAxisSpeed.Title.FontSpec.Size = 6;
            yAxisSpeed.Scale.FontSpec.Size = 6;
            comparisonGraph2.GraphPane.YAxisList.Add(yAxisSpeed);

            LineItem speedDataCurve = new LineItem("Speed Data", speed_Data, Color.Blue, SymbolType.None)
            { YAxisIndex = comparisonGraph2.GraphPane.YAxisList.IndexOf(yAxisSpeed) };
            comparisonGraph2.GraphPane.CurveList.Add(speedDataCurve);

            AxisLabel xAxisLabel = new AxisLabel("Time (Minutes)", "Arial", 10, Color.Black, false, false, false);
            comparisonGraph2.GraphPane.XAxis.Title = xAxisLabel;
            AxisLabel yAxisLabel = new AxisLabel("Unit", "Arial", 10, Color.Black, false, false, false);
            comparisonGraph2.GraphPane.YAxis.Title = yAxisLabel;
            comparisonGraph2.GraphPane.Title.Text = "File 2";

            comparisonGraph2.GraphPane.XAxis.Scale.Max = file2.list.Count / 60;
            comparisonGraph2.AxisChange();
            comparisonGraph2.Refresh();

        }

        private void comparisonGraph1_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            ///<summary>
            ///This method allows the first file to be zoomed, and then the statistics to change about the file
            ///This is similar to the previous method for zooming, however this method zooms two graphs
            ///The bottom section after the if statement, copies the zoom happening on file 1, to happen on file 2
            ///This allows the user to zoom both graphs at the same time, giving them an opportunity to spot the differences 
            /// </summary>
            
            file1.graph_list.Clear();
            int count = file1.list.Count;

            foreach (HR_Data_Row item in file1.list)
            {

                if (item.power < yAxisPower.Scale.Max && item.power > yAxisPower.Scale.Min && (item.count / 60) > comparisonGraph1.GraphPane.XAxis.Scale.Min && (item.count / 60) < comparisonGraph1.GraphPane.XAxis.Scale.Max)
                {
                    HR_Data_Row row;
                    row = item;
                    file1.graph_list.Add(row);
                }

                newState.ApplyState(comparisonGraph2.GraphPane);
                comparisonGraph2.Invalidate();
                comparisonGraph2.Refresh();
                Console.WriteLine("testing graph conversion 1 zoom");

            }

            comparisonList();

        }

        private void portionData1_ValueChanged(object sender, EventArgs e)
        {
            ///<summary>
            /// This method allows File 1's data to be split in to sections, such as data chunking
            /// This is done through lists and arrays. Spliting the data row in to the amount entered by the user
            /// Then displaying the information on a List View on Form 1
            /// </summary>
            
            int items_per_chunk = file1.graph_list.Count / Convert.ToInt32(portionData1.Value);
            int count = 0;

            List<HR_Data_Row>[] dataChunks = new List<HR_Data_Row>[Convert.ToInt32(portionData1.Value)];

            for (int i = 0; i < Convert.ToInt32(portionData1.Value); i++)
            {
                dataChunks[i] = new List<HR_Data_Row>();

                for (int j = 0; j < items_per_chunk; j++)
                {
                    dataChunks[i].Add(file1.graph_list[count]);
                    count = count + 1;
                }

            }

            portionDataListView.Clear();
            portionDataListView.Refresh();

            portionDataListView.Columns.Add("Chunk id:",75);
            portionDataListView.Columns.Add("Minimum Heart Rate:",125);
            portionDataListView.Columns.Add("Average Heart Rate:",125);
            portionDataListView.Columns.Add("Maximum Heart Rate:",125);
            portionDataListView.Columns.Add("Average Speed:",100);
            portionDataListView.Columns.Add("Maximum Speed:",100);
            portionDataListView.Columns.Add("Average Power:",100);
            portionDataListView.Columns.Add("Maximum Power:",100);
            portionDataListView.Columns.Add("Average Altitude:",100);
            portionDataListView.Columns.Add("Maximum Altitude:",100);
            portionDataListView.Columns.Add("Total Distance:",100);

            int count_2 = 1;
            foreach (List<HR_Data_Row> item in dataChunks)
            {
                String[] chunk_values_array = new String[11];

                chunk_values_array[0] = Convert.ToString(count_2);
                chunk_values_array[1] = Convert.ToString(file1.calculate_minimum_HeartRate(item));
                chunk_values_array[2] = Convert.ToString(file1.calculate_average_HeartRate(item));
                chunk_values_array[3] = Convert.ToString(file1.calculate_maximum_HeartRate(item));
                chunk_values_array[4] = Convert.ToString(file1.calculate_average_Speed(item));
                chunk_values_array[5] = Convert.ToString(file1.calculate_maximum_Speed(item));
                chunk_values_array[6] = Convert.ToString(file1.calculate_average_Power(item));
                chunk_values_array[7] = Convert.ToString(file1.calculate_maximum_Power(item));
                chunk_values_array[8] = Convert.ToString(file1.calculate_average_Altitude(item));
                chunk_values_array[9] = Convert.ToString(file1.calculate_maximum_Altitude(item));
                chunk_values_array[10] = Convert.ToString(file1.calculate_total_distance(item));

                ListViewItem listitem = new ListViewItem(chunk_values_array);
                portionDataListView.Items.Add(listitem);

                count_2 = count_2 + 1;
            }
            Console.WriteLine("testing portion");
           
        }

        private void filechunk_ValueChanged(object sender, EventArgs e)
        {
            ///<summary>
            /// This method allows both of the files data to be split in to sections, such as data chunking
            /// This is done through lists and arrays. Spliting the data row in to the amount entered by the user
            /// Then displaying the information on a List View with both sets of information and the difference between the data 
            /// </summary>
            
            int items_per_chunk_file1 = file1.graph_list.Count / Convert.ToInt32(filechunk.Value);
            int items_per_chunk_file2 = file2.graph_list.Count / Convert.ToInt32(filechunk.Value);
            int count = 0;

            List<HR_Data_Row>[] file1DataChunks = new List<HR_Data_Row>[Convert.ToInt32(filechunk.Value)];
            List<HR_Data_Row>[] file2DataChunks = new List<HR_Data_Row>[Convert.ToInt32(filechunk.Value)];

            for (int i = 0; i < Convert.ToInt32(filechunk.Value); i++)
            {
                file1DataChunks[i] = new List<HR_Data_Row>();

                for (int j = 0; j < items_per_chunk_file1; j++)
                {
                    file1DataChunks[i].Add(file1.graph_list[count]);
                    count = count + 1;
                }

            }

            int count2 = 0;
            for (int i = 0; i < Convert.ToInt32(filechunk.Value); i++)
            {
                file2DataChunks[i] = new List<HR_Data_Row>();

                for (int j = 0; j < items_per_chunk_file2; j++)
                {
                    file2DataChunks[i].Add(file2.graph_list[count2]);
                    count2 = count2 + 1;
                }

            }

            comparisonChunkListView.Clear();
            comparisonChunkListView.Refresh();

            comparisonChunkListView.Columns.Add("Chunk id:", 75);
            comparisonChunkListView.Columns.Add("Minimum Heart Rate:", 125);
            comparisonChunkListView.Columns.Add("Average Heart Rate:", 125);
            comparisonChunkListView.Columns.Add("Maximum Heart Rate:", 125);
            comparisonChunkListView.Columns.Add("Average Speed:", 100);
            comparisonChunkListView.Columns.Add("Maximum Speed:", 100);
            comparisonChunkListView.Columns.Add("Average Power:", 100);
            comparisonChunkListView.Columns.Add("Maximum Power:", 100);
            comparisonChunkListView.Columns.Add("Average Altitude:", 100);
            comparisonChunkListView.Columns.Add("Maximum Altitude:", 100);
            comparisonChunkListView.Columns.Add("Total Distance:", 100);

            int count3= 1;
            foreach (List<HR_Data_Row> item in file1DataChunks)
            {
                String[] file1_chunk_array = new String[11];

                file1_chunk_array[0] = Convert.ToString("File 1: " + count3);
                file1_chunk_array[1] = Convert.ToString(file1.calculate_minimum_HeartRate(item));
                file1_chunk_array[2] = Convert.ToString(file1.calculate_average_HeartRate(item));
                file1_chunk_array[3] = Convert.ToString(file1.calculate_maximum_HeartRate(item));
                file1_chunk_array[4] = Convert.ToString(file1.calculate_average_Speed(item));
                file1_chunk_array[5] = Convert.ToString(file1.calculate_maximum_Speed(item));
                file1_chunk_array[6] = Convert.ToString(file1.calculate_average_Power(item));
                file1_chunk_array[7] = Convert.ToString(file1.calculate_maximum_Power(item));
                file1_chunk_array[8] = Convert.ToString(file1.calculate_average_Altitude(item));
                file1_chunk_array[9] = Convert.ToString(file1.calculate_maximum_Altitude(item));
                file1_chunk_array[10] = Convert.ToString(file1.calculate_total_distance(item));

                ListViewItem listitem = new ListViewItem(file1_chunk_array);
                comparisonChunkListView.Items.Add(listitem);

                count3 = count3 + 1;
            }

            int count4 = 1;
            foreach (List<HR_Data_Row> item in file2DataChunks)
            {
                String[] file2_chunk_array = new String[11];

                file2_chunk_array[0] = Convert.ToString("File 2: " + count4);
                file2_chunk_array[1] = Convert.ToString(file2.calculate_minimum_HeartRate(item));
                file2_chunk_array[2] = Convert.ToString(file2.calculate_average_HeartRate(item));
                file2_chunk_array[3] = Convert.ToString(file2.calculate_maximum_HeartRate(item));
                file2_chunk_array[4] = Convert.ToString(file2.calculate_average_Speed(item));
                file2_chunk_array[5] = Convert.ToString(file2.calculate_maximum_Speed(item));
                file2_chunk_array[6] = Convert.ToString(file2.calculate_average_Power(item));
                file2_chunk_array[7] = Convert.ToString(file2.calculate_maximum_Power(item));
                file2_chunk_array[8] = Convert.ToString(file2.calculate_average_Altitude(item));
                file2_chunk_array[9] = Convert.ToString(file2.calculate_maximum_Altitude(item));
                file2_chunk_array[10] = Convert.ToString(file2.calculate_total_distance(item));

                ListViewItem listitem = new ListViewItem(file2_chunk_array);
                comparisonChunkListView.Items.Add(listitem);

                count4 = count4 + 1;
            }
            Console.WriteLine("testing portion");

            int count5 = 1;
            for (int i = 0; i < file1DataChunks.Length; i++)
            {
                String[] difference_chunk_array = new String[11];

                difference_chunk_array[0] = Convert.ToString("Difference: " + count5);
                difference_chunk_array[1] = Convert.ToString(Math.Round(file1.calculate_minimum_HeartRate(file1DataChunks[i]) - file2.calculate_minimum_HeartRate(file2DataChunks[i]),2));
                difference_chunk_array[2] = Convert.ToString(Math.Round(file1.calculate_average_HeartRate(file1DataChunks[i]) - file2.calculate_average_HeartRate(file2DataChunks[i]),2));
                difference_chunk_array[3] = Convert.ToString(Math.Round(file1.calculate_maximum_HeartRate(file1DataChunks[i]) - file2.calculate_maximum_HeartRate(file2DataChunks[i]),2));
                difference_chunk_array[4] = Convert.ToString(Math.Round(file1.calculate_average_Speed(file1DataChunks[i]) - file2.calculate_average_Speed(file2DataChunks[i]),2));
                difference_chunk_array[5] = Convert.ToString(Math.Round(file1.calculate_maximum_Speed(file1DataChunks[i]) - file2.calculate_maximum_Speed(file2DataChunks[i]),2));
                difference_chunk_array[6] = Convert.ToString(Math.Round(file1.calculate_average_Power(file1DataChunks[i]) - file2.calculate_average_Power(file2DataChunks[i]),2));
                difference_chunk_array[7] = Convert.ToString(Math.Round(file1.calculate_maximum_Power(file1DataChunks[i]) - file2.calculate_maximum_Power(file2DataChunks[i]),2));
                difference_chunk_array[8] = Convert.ToString(Math.Round(file1.calculate_average_Altitude(file1DataChunks[i]) - file2.calculate_average_Altitude(file2DataChunks[i]),2));
                difference_chunk_array[9] = Convert.ToString(Math.Round(file1.calculate_maximum_Altitude(file1DataChunks[i]) - file2.calculate_maximum_Altitude(file2DataChunks[i]),2));
                difference_chunk_array[10] = Convert.ToString(Math.Round(file1.calculate_total_distance(file1DataChunks[i]) - file2.calculate_total_distance(file2DataChunks[i]),2));

                ListViewItem listitem = new ListViewItem(difference_chunk_array);
                comparisonChunkListView.Items.Add(listitem);

                count5 = count5 + 1;
            }




        }

        private void button1_Click(object sender, EventArgs e)
        {
            ///<summary>
            /// This method takes the users entries from the higherLimit, lowerLimit and SmoothEffect numberboxes on the screen
            /// By entering the details it finds the intervals on the file above or below the limits entered
            /// The smoothing effect, allows the user to test what the intervals would be if the track was smoother by changing the average 
            /// The details are then displayed on a list view to the user, they can change the entries and refresh the data 
            /// </summary>
            int higher_lim = Convert.ToInt32(higherLimit.Value);
            int lower_lim = Convert.ToInt32(lowerLimit.Value);
            int smoothingEffect = Convert.ToInt32(smoothEffect.Value);
            Boolean interval_started = false;
            List<Interval> list = new List<Interval>();
            Interval currentInterval = new Interval();

            for (int i = smoothingEffect; i < file1.graph_list.Count; i++)
            {
                //double avg = (file1.graph_list[i].power + file1.graph_list[i - 1].power + file1.graph_list[i - 2].power) / 3;
                double sum = 0;
                for (int k = 0; k < smoothingEffect; k++)
                {
                    sum = sum + file1.graph_list[i-k].power;
                }
                double avg = sum / smoothingEffect;
                if (interval_started == false)
                {
                    if (avg > higher_lim)
                    {
                        Console.WriteLine("Start Interval");
                        interval_started = true;
                        currentInterval.Start = i;
                    }
                }
                if (interval_started == true)
                {
                    if (avg < lower_lim)
                    {
                        Console.WriteLine("End Interval");
                        interval_started = false;
                        currentInterval.End = i;
                        list.Add(currentInterval);
                        currentInterval = new Interval();
                    }
                }
            }

            intervalDetectionListView.Clear();
            intervalDetectionListView.Refresh();

            intervalDetectionListView.Columns.Add("Interval:",75);
            intervalDetectionListView.Columns.Add("Average Power:",150);
            intervalDetectionListView.Columns.Add("Average Speed:", 150);
            intervalDetectionListView.Columns.Add("Average HeartRate:", 150);
            intervalDetectionListView.Columns.Add("Average Altitude:", 150);

            int count = 0;
            foreach (Interval item in list)
            {
                item.findPoints(file1.graph_list);
                String[] interval_detection_array = new String[5];

                interval_detection_array[0] = Convert.ToString(count);
                interval_detection_array[1] = Convert.ToString(file1.calculate_average_Power(item.intervalPoints));
                interval_detection_array[2] = Convert.ToString(file1.calculate_average_Speed(item.intervalPoints));
                interval_detection_array[3] = Convert.ToString(file1.calculate_average_HeartRate(item.intervalPoints));
                interval_detection_array[4] = Convert.ToString(file1.calculate_average_Altitude(item.intervalPoints));


                ListViewItem listitem = new ListViewItem(interval_detection_array);
                intervalDetectionListView.Items.Add(listitem);

                count = count + 1;
            }

           
        }

    }
}
