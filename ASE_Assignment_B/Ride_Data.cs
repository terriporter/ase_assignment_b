﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace ASE_Assignment_B
{
    public class Ride_Data
    {
        int version;
        int monitor;
        String SMode;
        public DateTime date;
        public DateTime startTime;
        public TimeSpan length;
        public int interval;
        int upper1;
        int lower1;
        int upper2;
        int lower2;
        int upper3;
        int lower3;
        DateTime timer1;
        DateTime timer2;
        DateTime timer3;
        int activeLimit;
        public int maxHR;
        public int restHR;
        int startDelay;
        public int vo2max;
        public int weight;

        ///<value>
        /// The booleans beneath are used in the SMode decode, 
        /// the information from these are used in other methods to find the unit conversions.
        ///</value>
        Boolean check = false;
        Boolean speed_check = false;
        Boolean cadence_check = false;
        Boolean altitude_check = false;
        public Boolean power_check = false;
        Boolean leg_power_check = false;
        Boolean pedal_index_check = false;
        Boolean heart_rate_check = false;
        Boolean unit_conversion_check = false;
        Boolean air_pressure_check = false;

        public List<HR_Data_Row> list { get; set; } = new List<HR_Data_Row>();
        public List<HR_Data_Row> graph_list { get; set; } = new List<HR_Data_Row>();
        public Boolean UKUnitButton { get; set; }
        public Boolean USUnitButton { get; set; }

        public void ReadFile(String Filename)
        {
            ///<summary>
            ///  This method is toread the file line by line, and store the data to the variable
            ///  There is a try catch around the code for the file, and a catch exception to be displayed
            ///  At the end of the method, the SMode is sent off to be decoded 
            /// </summary>


            System.IO.StreamReader file = new System.IO.StreamReader(Filename);

            String readLine = file.ReadLine();

            try
            {
                if (readLine == "[Params]")
                {
                    readLine = file.ReadLine();

                }
                else
                {
                    check = true;
                }

                if (readLine.StartsWith("Version="))
                {
                    String[] test;
                    test = readLine.Split('=');
                    version = int.Parse(test[1]);

                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Monitor="))
                {
                    String[] test = readLine.Split('=');
                    monitor = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("SMode="))
                {
                    String[] test = readLine.Split('=');
                    SMode = (test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Date="))
                {
                    String pattern = ("yyyyMMdd");
                    String[] test = readLine.Split('=');
                    date = DateTime.ParseExact(test[1], pattern, null);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("StartTime="))
                {
                    String[] test = readLine.Split('=');
                    startTime = DateTime.ParseExact(test[1], "HH:mm:ss.f", null);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Length="))
                {
                    String[] test = readLine.Split('=');
                    length = TimeSpan.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Interval="))
                {
                    String[] test = readLine.Split('=');
                    interval = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Upper1="))
                {
                    String[] test = readLine.Split('=');
                    upper1 = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Lower1="))
                {
                    String[] test = readLine.Split('=');
                    lower1 = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Upper2="))
                {
                    String[] test = readLine.Split('=');
                    upper2 = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Lower2="))
                {
                    String[] test = readLine.Split('=');
                    lower2 = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Upper3="))
                {
                    String[] test = readLine.Split('=');
                    upper3 = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Lower3="))
                {
                    String[] test = readLine.Split('=');
                    lower3 = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Timer1="))
                {
                    String[] test = readLine.Split('=');
                    timer1 = DateTime.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Timer2="))
                {
                    String[] test = readLine.Split('=');
                    timer2 = DateTime.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Timer3="))
                {
                    String[] test = readLine.Split('=');
                    timer3 = DateTime.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("ActiveLimit="))
                {
                    String[] test = readLine.Split('=');
                    activeLimit = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("MaxHR="))
                {
                    String[] test = readLine.Split('=');
                    maxHR = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("RestHR="))
                {
                    String[] test = readLine.Split('=');
                    restHR = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("StartDelay="))
                {
                    String[] test = readLine.Split('=');
                    startDelay = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("VO2max="))
                {
                    String[] test = readLine.Split('=');
                    vo2max = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                readLine = file.ReadLine();

                if (readLine.StartsWith("Weight="))
                {
                    String[] test = readLine.Split('=');
                    weight = int.Parse(test[1]);
                }
                else
                {
                    check = true;
                }

                decode_SMode();
                check_HR_data(Filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show("There is an issue with the document you have chosen.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        } //end of readFile


        private void check_HR_data(String filePath)
        {
            ///<summary>
            ///This method checks the bottom part of the files, splits it in to sections
            /// Saves the specific parts of the file to seperate variables.
            /// The if statements surrounding the speed and altitiude are to determine the different units which an be used
            /// </summary>
            System.IO.StreamReader file = new System.IO.StreamReader(filePath);

            String readLine = file.ReadLine();

            while (file.ReadLine() != "[HRData]")
            {
                //Do nothing
            }

            readLine = file.ReadLine();
            int i = 0;
            while (readLine != null)
            {

                HR_Data_Row dataRow = new HR_Data_Row();

                String[] arr = readLine.Split(char.Parse("\t"));

                int column_count = 0;
               
                if(heart_rate_check == true)
                {
                    dataRow.heartRate = Convert.ToDouble(arr[column_count]);
                    column_count = column_count + 1;
                }
                
                if (speed_check == true)
                {
                    if (unit_conversion_check == false)
                    {
                        dataRow.speed_km = Math.Round(Convert.ToDouble(arr[column_count]) / 10, 2);
                        dataRow.speed_ms = Math.Round((dataRow.speed_km / 1.62137), 2);
                    }
                    else if (unit_conversion_check == true)
                    {
                        dataRow.speed_ms = Math.Round(Convert.ToDouble(arr[column_count]) / 10, 2);
                        dataRow.speed_km = Math.Round((dataRow.speed_ms * 0.62137), 2);
                    }

                    column_count = column_count + 1;
                }

                if (cadence_check == true)
                {
                    dataRow.cadence = Convert.ToDouble(arr[column_count]);
                    column_count = column_count + 1;
                }

                if (altitude_check == true)
                {
                    if (unit_conversion_check == false)
                    {
                        dataRow.altitude_m = Math.Round(Convert.ToDouble(arr[column_count]), 2);
                        dataRow.altitude_ft = Math.Round(dataRow.altitude_m * 3.28084, 2);

                    }
                    else if (unit_conversion_check == true)
                    {
                        dataRow.altitude_ft = Math.Round(Convert.ToDouble(arr[column_count]), 2);
                        dataRow.altitude_m = Math.Round(dataRow.altitude_ft / 3.28084, 2);
                    }

                    column_count = column_count + 1;
                }

                if(power_check == true)
                {
                    dataRow.power = Convert.ToDouble(arr[column_count]);
                    column_count = column_count + 1;
                }
                
                if(pedal_index_check == true)
                {
                    dataRow.powerBalance = Convert.ToDouble(arr[column_count]);
                    column_count = column_count + 1;
                }

                //converts the power balance to bits, which splits it in half
                Byte[] byte_Array = BitConverter.GetBytes((int)dataRow.powerBalance);
                // from this we can find the power in the left leg and right leg from the first half
                dataRow.powerLeft = byte_Array[0];
                dataRow.powerRight = (100 - byte_Array[0]);
                //in the second half we can find the pedal index
                dataRow.pedalIndex = byte_Array[1];

                int secondsIntoRide = i * interval;
                TimeSpan timeSpan = TimeSpan.FromSeconds(secondsIntoRide);
                dataRow.pointDuration = timeSpan;

                dataRow.count = i;

                list.Add(dataRow);
                readLine = file.ReadLine();

                i++;
            }

            HR_Data_Row[] array = list.ToArray();
            graph_list = array.ToList();
            Console.WriteLine("Hello World");


        }

        public PointPairList Get_HeartRate_data()
        {
            ///<summary>
            ///This method finds the points from the files for heart rate to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                pointPairList.Add(timeSpan.TotalMinutes, datarow.heartRate);
            }
            return pointPairList;
        }

        public PointPairList Get_Speed_data()
        {
            ///<summary>
            ///This method finds the points from the files for speed to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                if (UKUnitButton == true)
                {
                    pointPairList.Add(timeSpan.TotalMinutes, datarow.speed_km);
                }
                else if (USUnitButton == true)
                {
                    pointPairList.Add(timeSpan.TotalMinutes, datarow.speed_ms);
                }

            }
            return pointPairList;
        }

        public PointPairList Get_Cadence_data()
        {
            ///<summary>
            ///This method finds the points from the files for cadence to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                pointPairList.Add(timeSpan.TotalMinutes, datarow.cadence);
            }
            return pointPairList;
        }

        public PointPairList Get_Altitude_data()
        {
            ///<summary>
            ///This method finds the points from the files for altitude to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                if (UKUnitButton == true)
                {
                    pointPairList.Add(timeSpan.TotalMinutes, datarow.altitude_m);
                }
                else if (USUnitButton == true)
                {
                    pointPairList.Add(timeSpan.TotalMinutes, datarow.altitude_ft);
                }
            }
            return pointPairList;
        }

        public PointPairList Get_Power_data()
        {
            ///<summary>
            ///This method finds the points from the files for power to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                pointPairList.Add(timeSpan.TotalMinutes, datarow.power);
            }
            return pointPairList;
        }

        public PointPairList Get_PowerLeft_data()
        {
            ///<summary>
            ///This method finds the points from the files for power left to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                pointPairList.Add(timeSpan.TotalMinutes, datarow.powerLeft);
            }
            return pointPairList;
        }

        public PointPairList Get_PowerRight_data()
        {
            ///<summary>
            ///This method finds the points from the files for power right to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                pointPairList.Add(timeSpan.TotalMinutes, datarow.powerRight);
            }
            return pointPairList;
        }

        public PointPairList Get_PedalIndex_data()
        {
            ///<summary>
            ///This method finds the points from the files for pedal index to be plotted on the graph
            /// </summary>
            PointPairList pointPairList = new PointPairList();

            foreach (HR_Data_Row datarow in list)
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds(datarow.count);
                pointPairList.Add(timeSpan.TotalMinutes, datarow.pedalIndex);
            }
            return pointPairList;
        }

        public List<HR_Data_Row> HR_data()
        {
            return list;
        }

        public double calculate_average_HeartRate(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            /// This method calculates the average heartRate by looping through the file and collecting all the information
            /// then dividing it by the count from the loops
            ///</summary>
            int count = 0;
            double average_heartRate = 0;
            double total_heartRate = 0;

            foreach (HR_Data_Row datarow in graph_list)
            {
                total_heartRate = total_heartRate + datarow.heartRate;

                count++;
            }

            average_heartRate = Math.Round((total_heartRate / count), 2);
            return (average_heartRate);
        }

        public double calculate_maximum_HeartRate(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method calculates the maximum heartrate of the file
            ///By looping through and checking if the value is higher than the previous
            /// </summary>
            double maximum_heartRate = double.MinValue;

            foreach (HR_Data_Row datarow in graph_list)
            {
                if (datarow.heartRate > maximum_heartRate)
                {
                    maximum_heartRate = datarow.heartRate;
                }

            }

            return (maximum_heartRate);
        }

        public double calculate_minimum_HeartRate(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            /// This method calculates the minimum heartrate of the file
            /// By loopig through and checking if the value was lower than the previous
            /// </summary>
            double minimum_heartRate = double.MaxValue;

            foreach (HR_Data_Row datarow in graph_list)
            {
                if (datarow.heartRate < minimum_heartRate)
                {
                    minimum_heartRate = datarow.heartRate;
                }

            }

            return (minimum_heartRate);
        }

        public double calculate_average_Speed(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method calculates the average speed by looping through the file and calculating all the speed then diving by the count of the loops
            /// Speed can be in the UK or US version by the buttons on the form
            /// There are two different calculations for this with if statements around them
            /// </summary>
            int count = 0;
            double average_speed = 0;
            double total_speed = 0;

            foreach (HR_Data_Row datarow in graph_list)
            {
                // if the boolean for the UK unit is true, then the uk unit will be shown
                if (UKUnitButton == true)
                {
                    total_speed = total_speed + datarow.speed_km;

                    count++;
                }
                //if the boolean for the US unit is true, the the us unit will be shown
                else if (USUnitButton == true)
                {
                    total_speed = total_speed + datarow.speed_ms;

                    count++;
                }

            }

            average_speed = Math.Round((total_speed / count), 2);
            return (average_speed);
        }

        public double calculate_maximum_Speed(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method calculates the maximum speed by looping through the file for speed
            /// Speed can be in the UK or US version by the buttons on the form
            /// There are two different calculations for this with if statements around them
            /// </summary>
            double maximum_speed = double.MinValue;

            foreach (HR_Data_Row datarow in graph_list)
            {
                if (UKUnitButton == true)
                {
                    if (datarow.speed_km > maximum_speed)
                    {
                        maximum_speed = datarow.speed_km;
                    }

                }
                else if (USUnitButton == true)
                {
                    if (datarow.speed_ms > maximum_speed)
                    {
                        maximum_speed = datarow.speed_ms;
                    }
                }
            }

            return (maximum_speed);
        }

        public double calculate_average_Power(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method calculates the average power, by looking through the data and collecting all the information
            ///Then dividing the overall total by the count of the loops completed
            /// </summary>
            int count = 0;
            double average_power = 0;
            double total_power = 0;

            foreach (HR_Data_Row datarow in graph_list)
            {
                total_power = total_power + datarow.power;

                count++;
            }

            average_power = Math.Round((total_power / count), 2);
            return (average_power);
        }

        public double calculate_maximum_Power(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method finds the maximum power by looping through the file, and having an if statement to determine if the value is larger than the previous value
            /// </summary>
            double maximum_power = double.MinValue;

            foreach (HR_Data_Row datarow in graph_list)
            {
                if (datarow.power > maximum_power)
                {
                    maximum_power = datarow.power;
                }

            }

            return (maximum_power);
        }

        public double calculate_average_Altitude(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method calculates the average altitude by looping through the file for altitude
            /// Altitude can be in the UK or US version by the buttons on the form
            /// There are two different calculations for this with if statements around them
            /// </summary>
            int count = 0;
            double average_altitude = 0;
            double total_altitude = 0;

            foreach (HR_Data_Row datarow in graph_list)
            {
                if (UKUnitButton == true)
                {
                    total_altitude = total_altitude + datarow.altitude_m;

                    count++;
                }
                else if (USUnitButton == true)
                {
                    total_altitude = total_altitude + datarow.altitude_ft;

                    count++;
                }
            }

            average_altitude = Math.Round((total_altitude / count), 2);
            return (average_altitude);
        }

        public double calculate_maximum_Altitude(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            ///This method calculates the maximum altitude by looping through the file for altitude
            /// Altitude can be in the UK or US version by the buttons on the form
            /// There are two different calculations for this with if statements around them
            /// </summary>
            double maximum_altitude = double.MinValue;

            foreach (HR_Data_Row datarow in graph_list)
            {
                if (UKUnitButton == true)
                {
                    if (datarow.altitude_m > maximum_altitude)
                    {
                        maximum_altitude = datarow.altitude_m;
                    }

                }
                else if (USUnitButton == true)
                {
                    if (datarow.altitude_ft > maximum_altitude)
                    {
                        maximum_altitude = datarow.altitude_ft;
                    }
                }

            }

            return (maximum_altitude);
        }

        public double calculate_total_distance(List<HR_Data_Row> graph_list)
        {
            ///<summary>
            /// This method calculates the total distance of the file by calculating the speed and length of file
            /// </summary>
            double average_speed = 0;
            double total_distance = 0;

            average_speed = calculate_average_Speed(graph_list);

            total_distance = Math.Round((average_speed * length.TotalHours), 2);

            return total_distance;
        }

        public void decode_SMode()
        {
            ///<summary>
            /// The Smode is split in to chars
            /// Each char is read and marked with a boolean, 0 = false, 1 = true
            /// These booleans are used to determine which units are going to be used for the speed 
            /// </summary>
            char[] SMode_char = SMode.ToCharArray();

            //issue is here 

            if (SMode_char[0] == '1')
            {
                speed_check = true;
            }
            else
            {
                speed_check = false;
            }

            if (SMode_char[1] == '1')
            {
                cadence_check = true;
            }
            else
            {
                cadence_check = false;
            }

            if (SMode_char[2] == '1')
            {
                altitude_check = true;
            }
            else
            {
                altitude_check = false;
            }

            if (SMode_char[3] == '1')
            {
                power_check = true;
            }
            else
            {
                power_check = false;
            }

            if (SMode_char[4] == '1')
            {
                leg_power_check = true;
            }
            else
            {
                leg_power_check = false;
            }

            if (SMode_char[5] == '1')
            {
                pedal_index_check = true;
            }
            else
            {
                pedal_index_check = false;
            }

            if (SMode_char[6] == '1')
            {
                heart_rate_check = true;
            }
            else
            {
                heart_rate_check = false;
            }

            if (SMode_char[7] == '1')
            {
                unit_conversion_check = true;
            }
            else
            {
                unit_conversion_check = false;
            }

            /* if (SMode_char[8] == '1')
            {
                air_pressure_check = true;
            }
            else
            {
                air_pressure_check = false;
            } */

        }

        public void calculate_FTP(decimal ftp_value)
        {
            /// <summary>
            /// This takes the power from the HR_Data, divides by the value chosen by the user and turns it in to a percentage
            /// </summary>
            foreach (HR_Data_Row datarow in list)
            {
                datarow.percentage_ftp = Math.Round((datarow.power / Convert.ToDouble(ftp_value)) * 100, 2);
            }

        }

        public void calculate_HR_Percentage(decimal HR_value)
        {
            /// <summary>
            /// This takes the heart rate from the HR_Data, divides by the value chosen by the user and turns it in to a percentage
            /// </summary>
            foreach (HR_Data_Row datarow in list)
            {
                datarow.percentage_HR = Math.Round((datarow.heartRate / Convert.ToDouble(HR_value)) * 100, 2);
            }
        }

        public double calculate_Normalized_Power(List<HR_Data_Row> list)
        {
            ///<summary>
            /// The normalized power calculation is taken from the data in the file
            /// A list is created to save the averages from the data file
            /// Then goes through the calculation and returned to the list view
            /// </summary>
            List<double> average_list = new List<double>();

            int average_points = (30 / interval);

            for (int i = average_points; i < (list.Count - average_points); i++)
            {

                double sum = 0;

                for (int j = 0; j < average_points; j++)
                {
                    sum = sum + list[i + j].power;
                }

                double average = sum / average_points;
                double average_Power4 = Math.Pow(average,4);

                average_list.Add(average_Power4);
            }

            double total_averages = average_list.Average();

            total_averages = Math.Pow(total_averages, 1.0 / 4.0);

            return Math.Round(total_averages,2);
        }

        public double calculate_Intensity_Factor(double FTP, double normalized_power)
        {
            ///<summary>
            /// This method calculates the IF, which then needs to be used in other calculations
            /// The intensity factor is displayed on the list view on comparison
            /// The FTP is gathered from the users input on the screen 
            /// </summary>
            double intensity_factor = 0;

            intensity_factor = (normalized_power / FTP);

            return Math.Round(intensity_factor,2);
        }

        public double calculate_training_stress_score(double intensity_factor, double duration, double normalized_power, double FTP)
        {
            ///<summary>
            ///This method calculates the TSS, and returns the value to be displayed on a list view
            /// It takes the IF, duration in seconds, NP, and FTP which is entered from the user to calculate the TSS
            /// </summary>
            double TSS = 0;

            TSS = ((duration * normalized_power * intensity_factor) / (FTP * 3600)) * 100;
            return Math.Round(TSS,2);
        }

        
    }
}
