﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment_B
{
    public class Interval
    {
        ///<value>
        /// The values contain the start and end value for the interval
        /// A list to store the start and end interval information
        /// </value>
        public int Start { get; set; }
        public int End { get; set; }
        public List<HR_Data_Row> intervalPoints = new List<HR_Data_Row>();

        /// <summary>
        /// This method finds the start time, and the time of the interval
        /// through minusing the start from the end. 
        /// This is used for interval detection in Form1
        /// </summary>
        /// <param name="AllPoints"></param>
        public void findPoints(List <HR_Data_Row> AllPoints)
        {
            intervalPoints = AllPoints.GetRange(Start, (End - Start));

        }
    }
}
