﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASE_Assignment_B
{
    public class HR_Data_Row
    {
        ///<value>
        ///Below are the list of values in the HR Data section on the file 
        ///Each value is used within the Ride_Data class
        /// </value>
        public double heartRate { get; set; }
        public double speed_km { get; set; }
        public double speed_ms { get; set; }
        public double cadence { get; set; }
        public double altitude_m { get; set; }
        public double altitude_ft { get; set; }
        public double power { get; set; }
        public double powerLeft { get; set; }
        public double powerRight { get; set; }
        public double pedalIndex { get; set; }
        public double powerBalance { get; set; }
        public int count { get; set; }
        public double percentage_ftp { get; set; }
        public double percentage_HR { get; set; }
        public TimeSpan pointDuration { get; set; }
    }
}
