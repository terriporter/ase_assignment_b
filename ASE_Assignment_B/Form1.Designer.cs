﻿namespace ASE_Assignment_B
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Cycle_Graph = new ZedGraph.ZedGraphControl();
            this.bodyDataView = new System.Windows.Forms.ListView();
            this.statisticsListView = new System.Windows.Forms.ListView();
            this.bodyDataLabel = new System.Windows.Forms.Label();
            this.statisticsLabel = new System.Windows.Forms.Label();
            this.conversionLabel = new System.Windows.Forms.Label();
            this.UKUnitRadioButton = new System.Windows.Forms.RadioButton();
            this.USUnitRadioButton = new System.Windows.Forms.RadioButton();
            this.heartrateLabel = new System.Windows.Forms.Label();
            this.maxHR_numbox = new System.Windows.Forms.NumericUpDown();
            this.maxheartrate_minilabel = new System.Windows.Forms.Label();
            this.ftpLabel = new System.Windows.Forms.Label();
            this.ftp_minilabel = new System.Windows.Forms.Label();
            this.ftp_numBox = new System.Windows.Forms.NumericUpDown();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.graphComparisonList = new System.Windows.Forms.ListView();
            this.comparisonGraph2 = new ZedGraph.ZedGraphControl();
            this.comparisonGraph1 = new ZedGraph.ZedGraphControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.portionDataListView = new System.Windows.Forms.ListView();
            this.portionData1 = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.filechunk = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.comparisonChunkListView = new System.Windows.Forms.ListView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.higherLimit = new System.Windows.Forms.NumericUpDown();
            this.lowerLimit = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.intervalDetectionListView = new System.Windows.Forms.ListView();
            this.label7 = new System.Windows.Forms.Label();
            this.FTPenteredBox = new System.Windows.Forms.NumericUpDown();
            this.smoothEffect = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxHR_numbox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ftp_numBox)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portionData1)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filechunk)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.higherLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowerLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTPenteredBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smoothEffect)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1262, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem,
            this.openFileBToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.openFileToolStripMenuItem.Text = "Open File";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // openFileBToolStripMenuItem
            // 
            this.openFileBToolStripMenuItem.Name = "openFileBToolStripMenuItem";
            this.openFileBToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.openFileBToolStripMenuItem.Text = "Open File 2";
            this.openFileBToolStripMenuItem.Click += new System.EventHandler(this.openFileBToolStripMenuItem_Click);
            // 
            // Cycle_Graph
            // 
            this.Cycle_Graph.Location = new System.Drawing.Point(8, 187);
            this.Cycle_Graph.Name = "Cycle_Graph";
            this.Cycle_Graph.ScrollGrace = 0D;
            this.Cycle_Graph.ScrollMaxX = 0D;
            this.Cycle_Graph.ScrollMaxY = 0D;
            this.Cycle_Graph.ScrollMaxY2 = 0D;
            this.Cycle_Graph.ScrollMinX = 0D;
            this.Cycle_Graph.ScrollMinY = 0D;
            this.Cycle_Graph.ScrollMinY2 = 0D;
            this.Cycle_Graph.Size = new System.Drawing.Size(1199, 357);
            this.Cycle_Graph.TabIndex = 1;
            this.Cycle_Graph.UseExtendedPrintDialog = true;
            this.Cycle_Graph.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.Cycle_Graph_ZoomEvent);
            // 
            // bodyDataView
            // 
            this.bodyDataView.Location = new System.Drawing.Point(416, 29);
            this.bodyDataView.Name = "bodyDataView";
            this.bodyDataView.Size = new System.Drawing.Size(359, 148);
            this.bodyDataView.TabIndex = 2;
            this.bodyDataView.UseCompatibleStateImageBehavior = false;
            this.bodyDataView.View = System.Windows.Forms.View.Details;
            // 
            // statisticsListView
            // 
            this.statisticsListView.Location = new System.Drawing.Point(848, 29);
            this.statisticsListView.Name = "statisticsListView";
            this.statisticsListView.Size = new System.Drawing.Size(359, 148);
            this.statisticsListView.TabIndex = 3;
            this.statisticsListView.UseCompatibleStateImageBehavior = false;
            this.statisticsListView.View = System.Windows.Forms.View.Details;
            // 
            // bodyDataLabel
            // 
            this.bodyDataLabel.AutoSize = true;
            this.bodyDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bodyDataLabel.Location = new System.Drawing.Point(413, 2);
            this.bodyDataLabel.Name = "bodyDataLabel";
            this.bodyDataLabel.Size = new System.Drawing.Size(72, 15);
            this.bodyDataLabel.TabIndex = 4;
            this.bodyDataLabel.Text = "Body Data";
            // 
            // statisticsLabel
            // 
            this.statisticsLabel.AutoSize = true;
            this.statisticsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statisticsLabel.Location = new System.Drawing.Point(845, 2);
            this.statisticsLabel.Name = "statisticsLabel";
            this.statisticsLabel.Size = new System.Drawing.Size(65, 15);
            this.statisticsLabel.TabIndex = 5;
            this.statisticsLabel.Text = "Statistics";
            // 
            // conversionLabel
            // 
            this.conversionLabel.AutoSize = true;
            this.conversionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.conversionLabel.Location = new System.Drawing.Point(8, 3);
            this.conversionLabel.Name = "conversionLabel";
            this.conversionLabel.Size = new System.Drawing.Size(108, 15);
            this.conversionLabel.TabIndex = 6;
            this.conversionLabel.Text = "Unit Conversion";
            // 
            // UKUnitRadioButton
            // 
            this.UKUnitRadioButton.AutoSize = true;
            this.UKUnitRadioButton.Checked = true;
            this.UKUnitRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UKUnitRadioButton.Location = new System.Drawing.Point(8, 30);
            this.UKUnitRadioButton.Name = "UKUnitRadioButton";
            this.UKUnitRadioButton.Size = new System.Drawing.Size(126, 19);
            this.UKUnitRadioButton.TabIndex = 7;
            this.UKUnitRadioButton.TabStop = true;
            this.UKUnitRadioButton.Text = "UK Unit (KM/H  M)";
            this.UKUnitRadioButton.UseVisualStyleBackColor = true;
            this.UKUnitRadioButton.CheckedChanged += new System.EventHandler(this.UKUnitRadioButton_CheckedChanged);
            // 
            // USUnitRadioButton
            // 
            this.USUnitRadioButton.AutoSize = true;
            this.USUnitRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.USUnitRadioButton.Location = new System.Drawing.Point(154, 30);
            this.USUnitRadioButton.Name = "USUnitRadioButton";
            this.USUnitRadioButton.Size = new System.Drawing.Size(133, 19);
            this.USUnitRadioButton.TabIndex = 8;
            this.USUnitRadioButton.Text = "US Unit (MPH Feet)";
            this.USUnitRadioButton.UseVisualStyleBackColor = true;
            this.USUnitRadioButton.CheckedChanged += new System.EventHandler(this.USUnitRadioButton_CheckedChanged);
            // 
            // heartrateLabel
            // 
            this.heartrateLabel.AutoSize = true;
            this.heartrateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.heartrateLabel.Location = new System.Drawing.Point(8, 66);
            this.heartrateLabel.Name = "heartrateLabel";
            this.heartrateLabel.Size = new System.Drawing.Size(76, 15);
            this.heartrateLabel.TabIndex = 9;
            this.heartrateLabel.Text = "Heart Rate";
            this.heartrateLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // maxHR_numbox
            // 
            this.maxHR_numbox.Location = new System.Drawing.Point(142, 88);
            this.maxHR_numbox.Name = "maxHR_numbox";
            this.maxHR_numbox.Size = new System.Drawing.Size(120, 20);
            this.maxHR_numbox.TabIndex = 11;
            this.maxHR_numbox.ValueChanged += new System.EventHandler(this.maxHR_numbox_ValueChanged);
            // 
            // maxheartrate_minilabel
            // 
            this.maxheartrate_minilabel.AutoSize = true;
            this.maxheartrate_minilabel.Location = new System.Drawing.Point(8, 90);
            this.maxheartrate_minilabel.Name = "maxheartrate_minilabel";
            this.maxheartrate_minilabel.Size = new System.Drawing.Size(109, 13);
            this.maxheartrate_minilabel.TabIndex = 12;
            this.maxheartrate_minilabel.Text = "Maximum Heart Rate:";
            // 
            // ftpLabel
            // 
            this.ftpLabel.AutoSize = true;
            this.ftpLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ftpLabel.Location = new System.Drawing.Point(8, 125);
            this.ftpLabel.Name = "ftpLabel";
            this.ftpLabel.Size = new System.Drawing.Size(179, 15);
            this.ftpLabel.TabIndex = 13;
            this.ftpLabel.Text = "Funtional Threshold Power";
            this.ftpLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ftp_minilabel
            // 
            this.ftp_minilabel.AutoSize = true;
            this.ftp_minilabel.Location = new System.Drawing.Point(8, 145);
            this.ftp_minilabel.Name = "ftp_minilabel";
            this.ftp_minilabel.Size = new System.Drawing.Size(142, 13);
            this.ftp_minilabel.TabIndex = 15;
            this.ftp_minilabel.Text = "Functional Threshold Power:";
            // 
            // ftp_numBox
            // 
            this.ftp_numBox.Location = new System.Drawing.Point(167, 143);
            this.ftp_numBox.Name = "ftp_numBox";
            this.ftp_numBox.Size = new System.Drawing.Size(120, 20);
            this.ftp_numBox.TabIndex = 14;
            this.ftp_numBox.ValueChanged += new System.EventHandler(this.ftp_numBox_ValueChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1238, 582);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.conversionLabel);
            this.tabPage1.Controls.Add(this.Cycle_Graph);
            this.tabPage1.Controls.Add(this.statisticsLabel);
            this.tabPage1.Controls.Add(this.ftp_minilabel);
            this.tabPage1.Controls.Add(this.bodyDataLabel);
            this.tabPage1.Controls.Add(this.UKUnitRadioButton);
            this.tabPage1.Controls.Add(this.statisticsListView);
            this.tabPage1.Controls.Add(this.bodyDataView);
            this.tabPage1.Controls.Add(this.ftp_numBox);
            this.tabPage1.Controls.Add(this.USUnitRadioButton);
            this.tabPage1.Controls.Add(this.ftpLabel);
            this.tabPage1.Controls.Add(this.heartrateLabel);
            this.tabPage1.Controls.Add(this.maxheartrate_minilabel);
            this.tabPage1.Controls.Add(this.maxHR_numbox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1230, 556);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Main";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.FTPenteredBox);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.graphComparisonList);
            this.tabPage2.Controls.Add(this.comparisonGraph2);
            this.tabPage2.Controls.Add(this.comparisonGraph1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1230, 556);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Comparison";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Comparison of Statistics";
            // 
            // graphComparisonList
            // 
            this.graphComparisonList.Location = new System.Drawing.Point(6, 24);
            this.graphComparisonList.Name = "graphComparisonList";
            this.graphComparisonList.Size = new System.Drawing.Size(284, 430);
            this.graphComparisonList.TabIndex = 2;
            this.graphComparisonList.UseCompatibleStateImageBehavior = false;
            this.graphComparisonList.View = System.Windows.Forms.View.Details;
            // 
            // comparisonGraph2
            // 
            this.comparisonGraph2.Location = new System.Drawing.Point(305, 268);
            this.comparisonGraph2.Name = "comparisonGraph2";
            this.comparisonGraph2.ScrollGrace = 0D;
            this.comparisonGraph2.ScrollMaxX = 0D;
            this.comparisonGraph2.ScrollMaxY = 0D;
            this.comparisonGraph2.ScrollMaxY2 = 0D;
            this.comparisonGraph2.ScrollMinX = 0D;
            this.comparisonGraph2.ScrollMinY = 0D;
            this.comparisonGraph2.ScrollMinY2 = 0D;
            this.comparisonGraph2.Size = new System.Drawing.Size(912, 255);
            this.comparisonGraph2.TabIndex = 1;
            this.comparisonGraph2.UseExtendedPrintDialog = true;
            // 
            // comparisonGraph1
            // 
            this.comparisonGraph1.Location = new System.Drawing.Point(305, 6);
            this.comparisonGraph1.Name = "comparisonGraph1";
            this.comparisonGraph1.ScrollGrace = 0D;
            this.comparisonGraph1.ScrollMaxX = 0D;
            this.comparisonGraph1.ScrollMaxY = 0D;
            this.comparisonGraph1.ScrollMaxY2 = 0D;
            this.comparisonGraph1.ScrollMinX = 0D;
            this.comparisonGraph1.ScrollMinY = 0D;
            this.comparisonGraph1.ScrollMinY2 = 0D;
            this.comparisonGraph1.Size = new System.Drawing.Size(912, 256);
            this.comparisonGraph1.TabIndex = 0;
            this.comparisonGraph1.UseExtendedPrintDialog = true;
            this.comparisonGraph1.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.comparisonGraph1_ZoomEvent);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.portionDataListView);
            this.tabPage3.Controls.Add(this.portionData1);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1230, 556);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Data Chunking";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // portionDataListView
            // 
            this.portionDataListView.Location = new System.Drawing.Point(19, 63);
            this.portionDataListView.Name = "portionDataListView";
            this.portionDataListView.Size = new System.Drawing.Size(1164, 421);
            this.portionDataListView.TabIndex = 8;
            this.portionDataListView.UseCompatibleStateImageBehavior = false;
            this.portionDataListView.View = System.Windows.Forms.View.Details;
            // 
            // portionData1
            // 
            this.portionData1.Location = new System.Drawing.Point(114, 18);
            this.portionData1.Maximum = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.portionData1.Name = "portionData1";
            this.portionData1.Size = new System.Drawing.Size(120, 20);
            this.portionData1.TabIndex = 7;
            this.portionData1.ValueChanged += new System.EventHandler(this.portionData1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Portion Data:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.filechunk);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.comparisonChunkListView);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1230, 556);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Comparison Chunking";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // filechunk
            // 
            this.filechunk.Location = new System.Drawing.Point(293, 16);
            this.filechunk.Name = "filechunk";
            this.filechunk.Size = new System.Drawing.Size(120, 20);
            this.filechunk.TabIndex = 2;
            this.filechunk.ValueChanged += new System.EventHandler(this.filechunk_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(267, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "Comparison Chunking between two Files";
            // 
            // comparisonChunkListView
            // 
            this.comparisonChunkListView.Location = new System.Drawing.Point(19, 50);
            this.comparisonChunkListView.Name = "comparisonChunkListView";
            this.comparisonChunkListView.Size = new System.Drawing.Size(1176, 463);
            this.comparisonChunkListView.TabIndex = 0;
            this.comparisonChunkListView.UseCompatibleStateImageBehavior = false;
            this.comparisonChunkListView.View = System.Windows.Forms.View.Details;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.smoothEffect);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.higherLimit);
            this.tabPage5.Controls.Add(this.lowerLimit);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.button1);
            this.tabPage5.Controls.Add(this.intervalDetectionListView);
            this.tabPage5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1230, 556);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Interval Detection";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(913, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "Higher Detection Limit:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(913, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "Lower Detection Limit:";
            // 
            // higherLimit
            // 
            this.higherLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.higherLimit.Location = new System.Drawing.Point(1059, 102);
            this.higherLimit.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.higherLimit.Name = "higherLimit";
            this.higherLimit.Size = new System.Drawing.Size(120, 21);
            this.higherLimit.TabIndex = 4;
            this.higherLimit.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // lowerLimit
            // 
            this.lowerLimit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerLimit.Location = new System.Drawing.Point(1059, 63);
            this.lowerLimit.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.lowerLimit.Name = "lowerLimit";
            this.lowerLimit.Size = new System.Drawing.Size(120, 21);
            this.lowerLimit.TabIndex = 3;
            this.lowerLimit.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(123, 15);
            this.label4.TabIndex = 2;
            this.label4.Text = "Interval Detection ";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1074, 189);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Detect";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // intervalDetectionListView
            // 
            this.intervalDetectionListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.intervalDetectionListView.Location = new System.Drawing.Point(30, 43);
            this.intervalDetectionListView.Name = "intervalDetectionListView";
            this.intervalDetectionListView.Size = new System.Drawing.Size(839, 459);
            this.intervalDetectionListView.TabIndex = 0;
            this.intervalDetectionListView.UseCompatibleStateImageBehavior = false;
            this.intervalDetectionListView.View = System.Windows.Forms.View.Details;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 481);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Enter FTP Statistic:";
            // 
            // FTPenteredBox
            // 
            this.FTPenteredBox.Location = new System.Drawing.Point(129, 479);
            this.FTPenteredBox.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.FTPenteredBox.Name = "FTPenteredBox";
            this.FTPenteredBox.Size = new System.Drawing.Size(120, 20);
            this.FTPenteredBox.TabIndex = 5;
            this.FTPenteredBox.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // smoothEffect
            // 
            this.smoothEffect.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.smoothEffect.Location = new System.Drawing.Point(1059, 143);
            this.smoothEffect.Name = "smoothEffect";
            this.smoothEffect.Size = new System.Drawing.Size(120, 21);
            this.smoothEffect.TabIndex = 7;
            this.smoothEffect.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(925, 145);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 15);
            this.label8.TabIndex = 8;
            this.label8.Text = "Smoothing Average:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 574);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Duncan\'s Dynamic Data";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.maxHR_numbox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ftp_numBox)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.portionData1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filechunk)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.higherLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowerLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FTPenteredBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smoothEffect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private ZedGraph.ZedGraphControl Cycle_Graph;
        private System.Windows.Forms.ListView bodyDataView;
        private System.Windows.Forms.ListView statisticsListView;
        private System.Windows.Forms.Label bodyDataLabel;
        private System.Windows.Forms.Label statisticsLabel;
        private System.Windows.Forms.Label conversionLabel;
        private System.Windows.Forms.RadioButton UKUnitRadioButton;
        private System.Windows.Forms.RadioButton USUnitRadioButton;
        private System.Windows.Forms.Label heartrateLabel;
        private System.Windows.Forms.NumericUpDown maxHR_numbox;
        private System.Windows.Forms.Label maxheartrate_minilabel;
        private System.Windows.Forms.Label ftpLabel;
        private System.Windows.Forms.Label ftp_minilabel;
        private System.Windows.Forms.NumericUpDown ftp_numBox;
        private System.Windows.Forms.ToolStripMenuItem openFileBToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView comparisonListView;
        private ZedGraph.ZedGraphControl comparisonGraph2;
        private ZedGraph.ZedGraphControl comparisonGraph1;
        private System.Windows.Forms.ListView graphComparisonList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView portionDataListView;
        private System.Windows.Forms.NumericUpDown portionData1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.NumericUpDown filechunk;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView comparisonChunkListView;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ListView intervalDetectionListView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown higherLimit;
        private System.Windows.Forms.NumericUpDown lowerLimit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown FTPenteredBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown smoothEffect;
        private System.Windows.Forms.Label label8;
    }
}

