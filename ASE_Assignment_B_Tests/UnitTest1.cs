﻿using System;
using ASE_Assignment_B;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ASE_Assignment_B_Tests
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// The first seven tests, check that information from the document is read in correctly
        /// The tests are different due to the perameters entered 
        /// One test is checking on a boolean, one on a date format, another on a time format, one with a date and time format
        /// The others are checking that data is received and matches the file
        /// </summary>
        [TestMethod]
        public void checkSMode()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.power_check == true);
        }

        [TestMethod]
        public void checkDate()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            DateTime expectedDate = new DateTime(2013, 02, 05);
            Assert.AreEqual(expectedDate, file1.date);

        }

        [TestMethod]
        public void checkStartTime()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            DateTime expectedTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15,46,20);
            Assert.AreEqual(expectedTime, file1.startTime);
        }

        [TestMethod]
        public void checkLength()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            TimeSpan expectedTime = new TimeSpan(0, 1, 6, 18, 900);
            Assert.AreEqual(expectedTime, file1.length);
        }

        [TestMethod]
        public void checkInterval(){
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.interval == 1);
        }

        [TestMethod]
        public void checkVO2()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.vo2max == 48);
        }

        [TestMethod]
        public void checkWeight()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.weight == 66);
        }

        [TestMethod]
        public void checkMaxHeartRate()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.maxHR == 195);
        }

        [TestMethod]
        public void checkRestHeartRate()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.restHR == 46);
        }

        /// <summary>
        /// The next couple of tests, check methods within the Ride_Data Class
        /// they check that the statistics displayed on the form, are actually matched when being checked 
        /// They check the averages of the information gathered from the data entry
        /// </summary>
        [TestMethod]
        public void checkAverageHeartRate()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.calculate_average_HeartRate(file1.graph_list) == 144.05);
        }

        [TestMethod]
        public void checkAveragePower()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Assert.IsTrue(file1.calculate_average_Power(file1.graph_list) == 168.17);
        }
        /// <summary>
        /// These tests are checking against the methods created for the advanced metrics
        /// The details entered, are checked against an example on myBeckett which is accurate
        /// By completing these tests, you are checking the calculation is correct
        /// </summary>
        [TestMethod]
        public void checkIntensityFactor()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            double IF = file1.calculate_Intensity_Factor(300,232);
            Assert.IsTrue(Math.Round(IF,2) == 0.77);
        }

        [TestMethod]
        public void checkTSS()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            double TSS = file1.calculate_training_stress_score(0.77, 3978.90 ,232,300);
            Assert.IsTrue(Math.Round(TSS, 2) == 65.81);
        }

        [TestMethod]
        public void checkNormalizedPower()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            double NP = file1.calculate_Normalized_Power(file1.graph_list);
            Assert.IsTrue(Math.Round(NP, 2) == 231.98);
        }

        /// <summary>
        /// This test is to test the interval class and method
        /// This shows that a new start and end interval can be created. 
        /// That the method 'findPoints' works by taking the start interval from the end 
        /// </summary>
        [TestMethod]
        public void testIntervals()
        {
            Ride_Data file1 = new Ride_Data();
            file1.ReadFile("C:\\Users\\Terri Porter\\Desktop\\Duncan\'s Details\\ASDBExampleCycleComputerData.hrm");
            Interval interval = new Interval();
            interval.Start = 100;
            interval.End = 200;
            interval.findPoints(file1.graph_list);
            Assert.IsTrue(interval.intervalPoints.Count == 100);
        }

    }
}
